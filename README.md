Soft Potato
===========
Soft Potato is an intuitive client for your CouchPotato server, it allow you to add movie to your wanted list, see which movies you have. You can also manage your many servers

## To setup your client, it's simple : ##
* Add your server in settings with the url, port and login and password or api key, you can retrieve the api key with your login/password
* Active the server by swiping the switch.
* Then you can go the search page to obviously search movies, select a quality to add the movie.
* Go to Wanted page to see your wanted movies
* Go to Manage page to see your managed movies

### Note : Soft Potato is in no way affiliated with or endorsed by CouchPotato. ###
### Soft Potato need a CouchPotato server to work. ###

## Screenshots ##
![Screenshots 1](https://bytebucket.org/oppenheimer/soft-potato/raw/9229376e0799f9fc0c691c781d7bb9f7304c124c/Soft%20Potato/Screenshot_1.png)
![Screenshots 2](https://bytebucket.org/oppenheimer/soft-potato/raw/9229376e0799f9fc0c691c781d7bb9f7304c124c/Soft%20Potato/Screenshot_2.png)
![Screenshots 3](https://bytebucket.org/oppenheimer/soft-potato/raw/9229376e0799f9fc0c691c781d7bb9f7304c124c/Soft%20Potato/Screenshot_3.png)
![Screenshots 4](https://bytebucket.org/oppenheimer/soft-potato/raw/9229376e0799f9fc0c691c781d7bb9f7304c124c/Soft%20Potato/Screenshot_4.png)
![Screenshots 5](https://bytebucket.org/oppenheimer/soft-potato/raw/9229376e0799f9fc0c691c781d7bb9f7304c124c/Soft%20Potato/Screenshot_5.png)
![Screenshots 6](https://bytebucket.org/oppenheimer/soft-potato/raw/9229376e0799f9fc0c691c781d7bb9f7304c124c/Soft%20Potato/Screenshot_6.png)

Copyright 2014 Maxime Cattet

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.