//
//  SPMovieCollectionViewCell.m
//  Soft Potato
//
//  Created by Maxime Cattet on 21/03/2014.
//  Copyright (c) 2014 Maxime Cattet. All rights reserved.
//

#import "SPMovieCollectionViewCell.h"

@implementation SPMovieCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
