//
//  SPSettingsTableViewController.h
//  Soft Potato
//
//  Created by Maxime Cattet on 22/03/2014.
//  Copyright (c) 2014 Maxime Cattet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SPServerTableViewController.h"
#import "SPServersManager.h"
#import "SPServerTableViewCell.h"

@interface SPSettingsTableViewController : UITableViewController <UINavigationControllerDelegate> {
    NSMutableArray *servers;
    NSIndexPath *selectedRow;
}

- (IBAction)SwitchSwitched:(UISwitch*)sender;

@end
