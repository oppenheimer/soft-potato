//
//  SPServer.m
//  Soft Potato
//
//  Created by Maxime Cattet on 22/03/2014.
//  Copyright (c) 2014 Maxime Cattet. All rights reserved.
//

#import "SPServer.h"

@implementation SPServer


- (id)initWithName:(NSString*)name URL:(NSURL *)url port:(NSInteger)port login:(NSString *)login password:(NSString *)psswd apiKey:(NSString *)apiKey{
    if (self = [super init]){
        _name = name;
        _url = url;
        _port = port;
        _login = login;
        _password = psswd;
        _apiKey = apiKey;
    }
    return self;
}

- (id)initWithName:(NSString*)name URL:(NSURL*)url port:(NSInteger)port login:(NSString*)login password:(NSString*)psswd{
    if (self = [super init]){
        _name = name;
        _url = url;
        _port = port;
        _login = login;
        _password = psswd;
        _apiKey = @"";
    }
    return self;
}

- (id)initWithName:(NSString*)name URL:(NSURL *)url port:(NSInteger)port apiKey:(NSString *)apiKey{
    if (self = [super init]){
        _name = name;
        _url = url;
        _port = port;
        _apiKey = apiKey;
        _login = @"";
        _password = @"";
    }
    return self;
}

- (BOOL)retrieveApiKey{
    NSURL *getKeyURL = [_url URLByAppendingPathComponent:[NSString stringWithFormat:@"%d/getkey/?p=%@&u=%@", _port, [_password MD5], [_login MD5]]];
    NSDictionary *jsonObjects = [SPFunctions getJsonFromURL:getKeyURL];
    BOOL success = [jsonObjects[@"success"] isEqual:[NSNumber numberWithBool:YES]];
    if (success){
        _apiKey = jsonObjects[@"api_key"];
    }
    return success;
}

- (NSDictionary*)toNSDictionary{
    NSDictionary *dictionnary = [[NSDictionary alloc] initWithObjectsAndKeys:
                                 _name, @"name",
                                 _login, @"login",
                                 _password, @"password",
                                 [_url absoluteString], @"url",
                                 [NSString stringWithFormat:@"%d", _port], @"port",
                                 _apiKey, @"apikey", nil];
    return dictionnary;
}

- (NSURL*)getFullURL{
    
    if (_apiKey == nil || [_apiKey isEqualToString:@""]){
        [self retrieveApiKey];
    }
    NSString *url = [NSString stringWithFormat:@"%@:%d/api/%@", [_url absoluteString], _port, _apiKey];
    return [NSURL URLWithString:url];
}


@end
