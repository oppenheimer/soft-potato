//
//  SPServersManager.h
//  Soft Potato
//
//  Created by Maxime Cattet on 22/03/2014.
//  Copyright (c) 2014 Maxime Cattet. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SPServer.h"

@interface SPServersManager : NSObject

@property (strong, nonatomic) SPServer *currentServer;
@property (assign, nonatomic) NSInteger ID;
@property (strong, nonatomic) NSMutableArray *servers;

+ (SPServersManager*)shared;
- (void)setServers:(NSMutableArray *)servers;
- (void)loadServers;
- (void)saveServers;

@end
