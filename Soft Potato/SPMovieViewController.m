//
//  SPMovieViewController.m
//  Soft Potato
//
//  Created by Maxime Cattet on 22/03/2014.
//  Copyright (c) 2014 Maxime Cattet. All rights reserved.
//

#import "SPMovieViewController.h"

@interface SPMovieViewController ()

@end

@implementation SPMovieViewController

- (void)setMovie:(SPMovie *)movie{
    if (_movie != movie){
        _movie = movie;
        
        [self configureView];
    }
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CALayer *layer = [_imdbLabel layer];
    [layer setMasksToBounds:YES];
    [layer setCornerRadius:2.5];
    
    _plot.scrollEnabled = NO;
    
    [_movieTitle sizeToFit];
    
    [self configureView];
}

- (void)configureView{
    self.title = _movie.title;
    
    _movieTitle.text = _movie.title;
    _tagline.text = _movie.tagline;
    _year.text = [NSString stringWithFormat:@"%d", _movie.year];
    _rating.text = _movie.rating;
    _poster.image = _movie.poster;
    _quality.text = _movie.quality;
    _plot.text = _movie.plot;
    _plot.textColor = [UIColor whiteColor];
    
    NSArray *keys = [_movie actors].allKeys;
    NSString *text = @"";
    for (NSString *key in keys) {
        [text stringByAppendingFormat:@"%@ \n", [_movie actors][key]];
    }
    _actors.text = text;
    
    if ([_movie.quality isEqualToString:@"1080p"]){
        [_quality setBackgroundColor:[UIColor greenColor]];
    }
    else{
        [_quality setBackgroundColor:[UIColor blueColor]];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)TrashButtonTouched:(UIBarButtonItem *)sender {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Warning !" message:@"Do you really want to delete this movie ?" delegate:self cancelButtonTitle:@"Nope" otherButtonTitles:@"Yes", nil];
    [alertView show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1){
        NSString *str = [NSString stringWithFormat:@"%@/media.delete/?id=%d", [[[SPServersManager shared].currentServer getFullURL] absoluteString], _movie.libraryID];
        NSURL *moviesURL = [NSURL URLWithString:str];
        NSData *data = [NSData dataWithContentsOfURL:moviesURL];
        
        SBJsonParser *jsonParse = [[SBJsonParser alloc] init];
        NSDictionary *jsonObjects = [jsonParse objectWithData:data];
//        
//        do {
//            str = [NSString stringWithFormat:@"%@/media.get/?id=%d", [[[SPServersManager shared].currentServer getFullURL] absoluteString], _movie.libraryID];
//            moviesURL = [NSURL URLWithString:str];
//            data = [NSData dataWithContentsOfURL:moviesURL];
//            jsonParse = [[SBJsonParser alloc] init];
//            jsonObjects = [jsonParse objectWithData:data];
//        } while ([jsonObjects[@"success"]  isEqual: @"0"]);
        
//        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    
}
@end
