//
//  SPAppDelegate.m
//  Soft Potato
//
//  Created by Maxime Cattet on 21/03/2014.
//  Copyright (c) 2014 Maxime Cattet. All rights reserved.
//

#import "SPAppDelegate.h"

@implementation SPAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    UITabBarController *tabBarController = (UITabBarController *)self.window.rootViewController;
    UITabBar *tabBar = tabBarController.tabBar;
    
    tabBar.translucent = NO;
    
    [[UITabBar appearance] setTintColor:[UIColor whiteColor]];
    [[UITabBar appearance] setBackgroundImage:[UIImage imageNamed:@"UITabBar_Background"]];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
//    UITabBarItem *tabBarItem1 = tabBar.items[0];
    UITabBarItem *tabBarItem2 = tabBar.items[1];
    UITabBarItem *tabBarItem3 = tabBar.items[2];
    UITabBarItem *tabBarItem4 = tabBar.items[3];
    
//    tabBarItem1.title = @"";
    tabBarItem2.title = @"Wanted";
    tabBarItem3.title = @"Manage";
    tabBarItem4.title = @"Settings";
    
    // Set same image for demo
//    tabBarItem1.image = [UIImage imageNamed:@"fork"];
    tabBarItem2.image = [UIImage imageNamed:@"wanted"];
    tabBarItem3.image = [UIImage imageNamed:@"manage"];
    tabBarItem4.image = [UIImage imageNamed:@"settings"];
    
    [[SPServersManager shared] loadServers];
    
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    [[SPServersManager shared] saveServers];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
