//
//  SPWantedMoviesViewController.h
//  Soft Potato
//
//  Created by Maxime Cattet on 22/03/2014.
//  Copyright (c) 2014 Maxime Cattet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SPMovieCollectionViewCell.h"
#import "SPMovieViewController.h"
#import "SBJson.h"
#import "SPMovie.h"
#import "SPServer.h"
#import "SPServersManager.h"

@interface SPWantedMoviesViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>{
    IBOutlet UICollectionView *collectionView;
    
    SPServer *server;
    NSMutableArray *movies;
    SPMovie *sentMovie;
}

- (IBAction)RefreshTouch:(UIBarButtonItem *)sender;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicatorView;

@end
