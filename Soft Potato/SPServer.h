//
//  SPServer.h
//  Soft Potato
//
//  Created by Maxime Cattet on 22/03/2014.
//  Copyright (c) 2014 Maxime Cattet. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSString+MD5.h"
#import "SPFunctions.h"

@interface SPServer : NSObject {
    SPServer *currentServer;
}

@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSURL *url;
@property (assign, nonatomic) NSInteger port;
@property (strong, nonatomic) NSString *login;
@property (strong, nonatomic) NSString *password;
@property (strong, nonatomic) NSString *apiKey;

- (id)initWithName:(NSString*)name URL:(NSURL*)url port:(NSInteger)port login:(NSString*)login password:(NSString*)psswd apiKey:(NSString*)apiKey;
- (id)initWithName:(NSString*)name URL:(NSURL*)url port:(NSInteger)port login:(NSString*)login password:(NSString*)psswd;
- (id)initWithName:(NSString*)name URL:(NSURL*)url port:(NSInteger)port apiKey:(NSString*)apiKey;
- (BOOL)retrieveApiKey;
- (NSURL*)getFullURL;
- (NSDictionary *)toNSDictionary;

@end
