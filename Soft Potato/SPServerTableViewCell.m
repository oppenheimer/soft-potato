//
//  SPServerTableViewCell.m
//  Soft Potato
//
//  Created by Maxime Cattet on 23/03/2014.
//  Copyright (c) 2014 Maxime Cattet. All rights reserved.
//

#import "SPServerTableViewCell.h"

@implementation SPServerTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
