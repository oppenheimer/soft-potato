//
//  SPServerTableViewController.m
//  Soft Potato
//
//  Created by Maxime Cattet on 22/03/2014.
//  Copyright (c) 2014 Maxime Cattet. All rights reserved.
//

#import "SPServerTableViewController.h"

@interface SPServerTableViewController ()

@end

@implementation SPServerTableViewController

- (void)setServer:(SPServer *)server andID:(NSInteger)ID{
    if (_server != server){
        _server = server;
        serverID = ID;
    }
//    [self configureView];
}

- (IBAction)EditButtonTouch:(id)sender {
    [self Edit];
}

- (IBAction)SaveButtonTouch:(id)sender {
    [self save];
    [self Edit];
}

- (IBAction)getAPIKeyTouch:(UIButton *)sender {
    NSDictionary *json = [SPFunctions getJsonFromURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@:%@/getkey/?p=%@&u=%@", url.text, port.text, [password.text MD5], [login.text MD5]]]];
    if ([json[@"success"] boolValue]){
        apikey.text = json[@"api_key"];
        [self save];
    }
    else{
        UIAlertView *doneAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Wrong URL or Login or Password" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [doneAlert show];
    }
}

- (IBAction)DeleteServerTouched:(id)sender {
    [self delete];
}

- (void)save{
    _server.name = name.text;
    _server.url = [NSURL URLWithString:url.text];
    _server.port = [port.text integerValue];
    _server.login = login.text;
    _server.password = password.text;
    _server.apiKey = apikey.text;
    [SPServersManager shared].servers[serverID] = _server;
    [[SPServersManager shared] saveServers];
}

- (void)delete{
    [[SPServersManager shared].servers removeObjectAtIndex:serverID];
    [[SPServersManager shared] saveServers];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)Edit{
    UIBarButtonItem *barButtonItem = nil;
    if (edit){
        edit = false;
        barButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(EditButtonTouch:)];
        self.navigationItem.rightBarButtonItem = barButtonItem;
        
        name.enabled = false;
        url.enabled = false;
        port.enabled = false;
        apikey.enabled = false;
        login.enabled = false;
        password.enabled = false;
        
        [name setTextColor:[UIColor lightGrayColor]];
        [url setTextColor:[UIColor lightGrayColor]];
        [port setTextColor:[UIColor lightGrayColor]];
        [apikey setTextColor:[UIColor lightGrayColor]];
        [login setTextColor:[UIColor lightGrayColor]];
        [password setTextColor:[UIColor lightGrayColor]];
        
        name.text = _server.name;
        url.text = [_server.url absoluteString];
        port.text = [NSString stringWithFormat:@"%d", _server.port];
        apikey.text = _server.apiKey;
        login.text = _server.login;
        password.text = _server.password;
        
        saveTableViewCell.hidden = true;
        deleteTableViewCell.hidden = true;
    }
    else{ // EDIT mode
        edit = true;
        barButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(EditButtonTouch:)];
        [barButtonItem setTintColor:[UIColor colorWithRed:83.0 green:137.0 blue:195.0 alpha:1.0]];
        self.navigationItem.rightBarButtonItem = barButtonItem;
        
        name.enabled = true;
        url.enabled = true;
        port.enabled = true;
        apikey.enabled = true;
        login.enabled = true;
        password.enabled = true;
        
        [name setTextColor:[UIColor whiteColor]];
        [url setTextColor:[UIColor whiteColor]];
        [port setTextColor:[UIColor whiteColor]];
        [apikey setTextColor:[UIColor whiteColor]];
        [login setTextColor:[UIColor whiteColor]];
        [password setTextColor:[UIColor whiteColor]];
        
        saveTableViewCell.hidden = false;
        deleteTableViewCell.hidden = false;
    }
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
//    [self configureView];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidAppear:(BOOL)animated{
    [self configureView];
}

- (void)configureView{
    if (_server.name != nil){
        self.title = _server.name;
        name.text = _server.name;
        url.text = [_server.url absoluteString];
        port.text = [NSString stringWithFormat:@"%d", _server.port];
        apikey.text = _server.apiKey;
        login.text = _server.login;
        password.text = _server.password;
        
        edit = true;
        [self Edit];
    }
    else{
        edit = false;
        [self Edit];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
////#warning Potentially incomplete method implementation.
//    // Return the number of sections.
//    return 3;
//}

//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
//{
////#warning Incomplete method implementation.
//    // Return the number of rows in the section.
//    return 3;
//}
/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
