//
//  SPSearchTableViewController.m
//  Soft Potato
//
//  Created by Maxime Cattet on 24/03/2014.
//  Copyright (c) 2014 Maxime Cattet. All rights reserved.
//

#import "SPSearchTableViewController.h"

@interface SPSearchTableViewController ()

@end

@implementation SPSearchTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _searchTextField.delegate = self;
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [searchResults count];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [_searchTextField  resignFirstResponder];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    self.activityIndicatorView = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(150, 175, 20, 20)];
    [self.activityIndicatorView setBackgroundColor:[UIColor clearColor]];
    [self.activityIndicatorView setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhite];
    [self.view addSubview:self.activityIndicatorView];
    [self.activityIndicatorView startAnimating];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^
                   {
                       //do something expensive
                       searchResults = [[NSMutableArray alloc] init];
                       NSString *str = [NSString stringWithFormat:@"%@/search/?q=%@", [[[[SPServersManager shared] currentServer] getFullURL] absoluteString], [_searchTextField.text stringByReplacingOccurrencesOfString:@" " withString:@"_"]];
                       NSURL *moviesURL = [NSURL URLWithString:str];
                       if ([SPFunctions connectedToInternetWithURL:moviesURL] == NO){
                           NSData *data = [NSData dataWithContentsOfURL:moviesURL];
                           SBJsonParser *jsonParse = [[SBJsonParser alloc] init];
                           NSDictionary *jsonObjects = [jsonParse objectWithData:data];
                           
                           for (NSDictionary *movie in jsonObjects[@"movies"]) {
                               [searchResults addObject:[[SPMovie alloc] initWithNSDictionnary:movie]];
                           }
                           
                           [self.tableView reloadData];
                       }
                       
                       //dispatch back to the main (UI) thread to stop the activity indicator
                       dispatch_async(dispatch_get_main_queue(), ^
                                      {
                                          [self.activityIndicatorView stopAnimating];
                                          [self.tableView reloadData];
                                      });
                   });
    
    
    return YES;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SPMovieSearchTableViewCell *cell = (SPMovieSearchTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"Movie" forIndexPath:indexPath];
    SPMovie *movie = searchResults[indexPath.row];
    cell.name.text = movie.title;
    cell.year.text = [NSString stringWithFormat:@"%d", movie.year];
    cell.rating.text = movie.rating;
    
    [self downloadImageWithURL:[movie posterURL] completionBlock:^(BOOL succeeded, UIImage *image){
        if (succeeded){
            cell.poster.image = image;
            movie.poster = image;
        }
    }];
    
    return cell;
}

- (void)downloadImageWithURL:(NSURL *)url completionBlock:(void (^)(BOOL, UIImage *))completionBlock{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
                               if (!error) {
                                   UIImage *image = [[UIImage alloc] initWithData:data];
                                   completionBlock(YES, image);
                               }
                               else
                                   completionBlock(NO,nil);
                           }];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSLog(@"%d", buttonIndex);
    
    if (buttonIndex != qualities.count){
        NSDictionary *quality = qualities[buttonIndex];
        NSString *str = [NSString stringWithFormat:@"%@/movie.add/?identifier=%@&profile_id=%@", [[SPServersManager shared].currentServer getFullURL], selectedMovie.imdb, quality[@"id"]];
        NSURL *url = [NSURL URLWithString:str];
        NSData *data = [NSData dataWithContentsOfURL:url];
        SBJsonParser *jsonParse = [[SBJsonParser alloc] init];
        NSDictionary *jsonObjects = [jsonParse objectWithData:data];
        
        if ([jsonObjects[@"success"] boolValue]){
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Movie" message:@"Movie added" delegate:self cancelButtonTitle:nil otherButtonTitles:@"ok", nil];
            [alertView show];
        }
    }
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    selectedMovie = searchResults[indexPath.row];
    
    UIActionSheet *popupQuery = [[UIActionSheet alloc] initWithTitle:@"Title" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles: nil];
    qualities = [[NSMutableArray alloc] init];
    NSString *str = [NSString stringWithFormat:@"%@/profile.list", [[[[SPServersManager shared] currentServer] getFullURL] absoluteString]];
    NSURL *moviesURL = [NSURL URLWithString:str];
    if ([SPFunctions connectedToInternetWithURL:moviesURL] == NO){
        NSData *data = [NSData dataWithContentsOfURL:moviesURL];
        SBJsonParser *jsonParse = [[SBJsonParser alloc] init];
        NSDictionary *jsonObjects = [jsonParse objectWithData:data];
        
        for (NSDictionary *quality in jsonObjects[@"list"]) {
            [popupQuery addButtonWithTitle:quality[@"label"]];
            [qualities addObject:quality];
        }
        [popupQuery addButtonWithTitle:@"Cancel"];
        popupQuery.cancelButtonIndex = qualities.count;
        
        [self.tableView reloadData];
    }
    
//    UIActionSheet *popupQuery = [[UIActionSheet alloc] initWithTitle:@"Title" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Other Button 1", @"Other Button 2", nil];
    popupQuery.actionSheetStyle = UIActionSheetStyleBlackOpaque;
    [popupQuery showFromTabBar:self.tabBarController.tabBar];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
}

- (IBAction)searchTextFieldEditingChanged:(UITextField *)sender {
    
}
@end
