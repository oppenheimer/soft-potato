//
//  SPWantedMoviesViewController.m
//  Soft Potato
//
//  Created by Maxime Cattet on 22/03/2014.
//  Copyright (c) 2014 Maxime Cattet. All rights reserved.
//

#import "SPWantedMoviesViewController.h"

@interface SPWantedMoviesViewController ()

@end

@implementation SPWantedMoviesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    collectionView.delegate = self;
    collectionView.dataSource = self;
}

- (void)viewDidAppear:(BOOL)animated{
    if ([[SPServersManager shared] currentServer] != server || server == nil){
        server = [[SPServersManager shared] currentServer];
        [self download];
    }
}

- (void)download{
    self.activityIndicatorView = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(150, 274, 20, 20)];
    [self.activityIndicatorView setBackgroundColor:[UIColor clearColor]];
    [self.activityIndicatorView setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhite];
    [self.view addSubview:self.activityIndicatorView];
    [self.activityIndicatorView startAnimating];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^
                   {
                       //do something expensive
                       [self downloadData];
                       
                       //dispatch back to the main (UI) thread to stop the activity indicator
                       dispatch_async(dispatch_get_main_queue(), ^
                                      {
                                          [self.activityIndicatorView stopAnimating];
                                          [collectionView reloadData];
                                      });
                   });
}

- (void)downloadData{
    movies = [[NSMutableArray alloc] init];
    NSString *str = [NSString stringWithFormat:@"%@/media.list/?status=active", [[server getFullURL] absoluteString]];
    NSURL *moviesURL = [NSURL URLWithString:str];
    if ([SPFunctions connectedToInternetWithURL:moviesURL] == NO){
        NSData *data = [NSData dataWithContentsOfURL:moviesURL];
        SBJsonParser *jsonParse = [[SBJsonParser alloc] init];
        NSDictionary *jsonObjects = [jsonParse objectWithData:data];
        
        for (NSDictionary *movie in jsonObjects[@"movies"]) {
            [movies addObject:[[SPMovie alloc] initWithNSDictionnary:movie]];
        }
    }
}

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    return [movies count];
}
// 2
- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return 1;
}
// 3
- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    SPMovieCollectionViewCell *cell = (SPMovieCollectionViewCell*)[cv dequeueReusableCellWithReuseIdentifier:@"MovieCell" forIndexPath:indexPath];
    SPMovie *movie = movies[indexPath.row];
    cell.title.text = [movie title];
    
    [self downloadImageWithURL:[movie posterURL] completionBlock:^(BOOL succeeded, UIImage *image){
        if (succeeded){
            cell.poster.image = image;
            movie.poster = image;
        }
    }];
    
//    [self downloadImageWithURL:[movie backdropURL] completionBlock:^(BOOL succeeded, UIImage *image){
//        if (succeeded){
//            movie.backdrop = image;
//        }
//    }];
    
    cell.backgroundColor = [UIColor blackColor];
    return cell;
}

- (void)downloadImageWithURL:(NSURL *)url completionBlock:(void (^)(BOOL, UIImage *))completionBlock{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
                               if (!error) {
                                   UIImage *image = [[UIImage alloc] initWithData:data];
                                   completionBlock(YES, image);
                               }
                               else
                                   completionBlock(NO,nil);
                           }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

 // In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"ShowMovie"]) {
        NSIndexPath *indexPath = [collectionView indexPathForCell:sender];
        sentMovie = movies[indexPath.row];
        [[segue destinationViewController] setMovie:sentMovie];
    }
}

- (IBAction)RefreshTouch:(UIBarButtonItem *)sender {
    [self download];
}
@end
