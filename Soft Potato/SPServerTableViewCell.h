//
//  SPServerTableViewCell.h
//  Soft Potato
//
//  Created by Maxime Cattet on 23/03/2014.
//  Copyright (c) 2014 Maxime Cattet. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPServerTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *serverName;
@property (strong, nonatomic) IBOutlet UISwitch *serverSwitch;

@end
